# SQL-запросы для ClickHouse

**Задача** 
Написать SQL-запросы для ClickHouse:

1. Выборки всех уникальных eventType у которых более 1000 событий.
2. Выборки событий которые произошли в первый день каждого месяца.
3. Выборки пользователей которые совершили более 3 различных eventType.

## Запросы

### 1. Выборка уникальных eventType с более чем 1000 событий

**Простая выборка:**

```sql
SELECT eventType
FROM events
GROUP BY eventType
HAVING count(*) > 1000;
```

**Выборка с ограничением по времени (за январь 2024 года):**

```sql
SELECT eventType
FROM events
WHERE eventTime BETWEEN '2024-01-01' AND '2024-01-31'
GROUP BY eventType
HAVING count(*) > 1000;
```

### 2. Выборка событий, произошедших в первый день каждого месяца

**Выборка всех событий первого числа текущего месяца:**

```sql
SELECT *
FROM events
WHERE toDate(eventTime) = toStartOfMonth(eventTime);
```

**Выборка уникальных событий первого числа каждого месяца за год (2024 год):**

```sql
SELECT DISTINCT eventType
FROM events
WHERE toDate(eventTime) = toStartOfMonth(eventTime)
AND toYear(eventTime) = 2024;
```

### 3. Выборка пользователей, совершивших более 3 различных eventType

**Выборка за весь период:**

```sql
SELECT userID
FROM events
GROUP BY userID
HAVING count(DISTINCT eventType) > 3;
```

**Выборка за последний месяц:**

```sql
SELECT userID
FROM events
WHERE eventTime >= now() - INTERVAL 1 MONTH
GROUP BY userID
HAVING count(DISTINCT eventType) > 3;
```
