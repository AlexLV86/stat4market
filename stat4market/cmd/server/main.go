package main

import (
	"fmt"
	"log"
	"tests/stat4market/stat4market/internal/config"
	"tests/stat4market/stat4market/internal/handlers"
	"tests/stat4market/stat4market/internal/services"
	"tests/stat4market/stat4market/internal/storage/clickhouse"

	_ "github.com/ClickHouse/clickhouse-go"
)

func main() {
	cfg, err := config.New()
	if err != nil {
		log.Fatalf("Error when reading config : %v", err)
	}
	fmt.Println(cfg)

	ch, err := clickhouse.New(cfg.ClickHouseDSN, cfg.BatchSize, cfg.BatchInterval)
	if err != nil {
		log.Fatalf("Error when creating clickhouse storage : %v", err)
	}
	eventService := services.NewEventService(ch)

	h := handlers.New(eventService)

	go h.Start(cfg.Port)
	h.Stop([]handlers.Closer{ch})
}
