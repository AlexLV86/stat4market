package models

type Event struct {
	EventID   int64  `json:"eventID"` // Идентификатор события
	EventType string `json:"eventType" binding:"required"`
	UserID    int64  `json:"userID" binding:"required"`
	EventTime string `json:"eventTime" binding:"required"`
	Payload   string `json:"payload" binding:"required"`
}
