package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Port          string        `envconfig:"PORT" default:"3000"`
	ClickHouseDSN string        `envconfig:"CLICKHOUSE_DSN" default:"tcp://localhost:9000?debug=false"`
	BatchSize     int           `envconfig:"BATCH_SIZE" default:"1000"`
	BatchInterval time.Duration `envconfig:"BATCH_INTERVAL" default:"1s"`
}

func New() (*Config, error) {
	c := &Config{}
	if err := envconfig.Process("", c); err != nil {
		return nil, err
	}
	return c, nil
}
