package handlers

import (
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"tests/stat4market/stat4market/internal/services"

	"time"

	"github.com/gin-gonic/gin"
)

type Closer interface {
	Close() error
}

type Handler struct {
	engine       *gin.Engine
	eventService *services.EventService
}

func New(eventService *services.EventService) *Handler {

	handler := &Handler{eventService: eventService}

	handler.initRoutes()

	return handler
}

func (h *Handler) initRoutes() {
	router := gin.New()

	router.Use(gin.Recovery())

	router.POST("/api/event", h.Event)

	h.engine = router
}

func (h *Handler) Start(port string) {
	httpServer := &http.Server{
		Addr:         ":" + port,
		Handler:      h.engine,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	err := httpServer.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func (h *Handler) Stop([]Closer) {
	// Бесконечное ожидание с помощью канала сигналов
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	// Ожидаем сигнал
	<-sigChan
	for _, closer := range []Closer{} {
		if err := closer.Close(); err != nil {
			log.Printf("Error when closing: %v", err)
		}
	}
	log.Println("Server stopped")
}
