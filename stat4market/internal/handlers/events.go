package handlers

import (
	"log"
	"tests/stat4market/stat4market/internal/models"

	"github.com/gin-gonic/gin"
)

func (h *Handler) Event(c *gin.Context) {
	var event models.Event
	if err := c.BindJSON(&event); err != nil {
		log.Printf("Error when binding json : %v", err)
		c.JSON(400, gin.H{"error": "bad request"})
		return
	}

	if err := h.eventService.SaveEvent(event); err != nil {
		log.Printf("Error when saving event : %v", err)
		c.JSON(500, gin.H{"error": "internal server error"})
		return
	}

	c.JSON(200, gin.H{"message": "ok"})
}
