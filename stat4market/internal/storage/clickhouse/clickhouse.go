package clickhouse

import (
	"database/sql"
	"log"
	"sync"
	"time"

	"tests/stat4market/stat4market/internal/storage"
)

type ClickHouseStorage struct {
	db            *sql.DB
	batchSize     int
	batchInterval time.Duration
	events        []storage.Event
	mu            sync.Mutex
	ticker        *time.Ticker
	done          chan bool
}

func New(dsn string, batchSize int, batchInterval time.Duration) (*ClickHouseStorage, error) {
	db, err := sql.Open("clickhouse", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}

	storage := &ClickHouseStorage{
		db:            db,
		batchSize:     batchSize,
		batchInterval: batchInterval,
		events:        make([]storage.Event, 0, batchSize),
		ticker:        time.NewTicker(batchInterval),
		done:          make(chan bool),
	}

	go storage.BatchTimer()

	return storage, nil
}

func (s *ClickHouseStorage) SaveEvent(event storage.Event) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.events = append(s.events, event)

	if len(s.events) >= s.batchSize {
		return s.batchInsert()
	}

	return nil
}

func (s *ClickHouseStorage) batchInsert() error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO events (eventID, eventType, userID, eventTime, payload) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}

	for _, event := range s.events {
		if _, err := stmt.Exec(event.EventID, event.EventType, event.UserID, event.EventTime, event.Payload); err != nil {
			tx.Rollback() // Откат в случае ошибки
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	// Очистка буфера событий после успешного коммита.
	s.events = s.events[:0]
	return nil
}

func (s *ClickHouseStorage) BatchTimer() {
	for {
		select {
		case <-s.ticker.C:
			s.mu.Lock()
			if len(s.events) > 0 {
				if err := s.batchInsert(); err != nil {
					log.Printf("failed to flush events: %v", err)
				}
			}
			s.mu.Unlock()
		case <-s.done:
			s.ticker.Stop()
			return
		}
	}
}

func (s *ClickHouseStorage) Close() error {
	close(s.done) // Сигнализируем остановку
	s.mu.Lock()
	defer s.mu.Unlock()
	if len(s.events) > 0 {
		if err := s.batchInsert(); err != nil {
			return err
		}
	}
	return s.db.Close()
}
