package storage

import "time"

type Storage interface {
	// SaveEvent сохраняет событие в хранилище.
	SaveEvent(event Event) error
	Close() error
}

// Event представляет данные события для хранения в базе данных.
type Event struct {
	EventID   int64     `db:"eventID"`
	EventType string    `db:"eventType"`
	UserID    int64     `db:"userID"`
	EventTime time.Time `db:"eventTime"`
	Payload   string    `db:"payload"`
}
