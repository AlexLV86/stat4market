package services

import (
	"tests/stat4market/stat4market/internal/models"
	"tests/stat4market/stat4market/internal/storage"
	"time"
)

type EventService struct {
	storage storage.Storage
}

func NewEventService(storage storage.Storage) *EventService {
	return &EventService{
		storage: storage,
	}
}

func (s *EventService) SaveEvent(event models.Event) error {
	eventTime, err := time.Parse("2006-01-02 15:04:05", event.EventTime)
	if err != nil {
		return err
	}

	e := storage.Event{
		EventType: event.EventType,
		UserID:    event.UserID,
		EventTime: eventTime,
		Payload:   event.Payload,
	}
	e.EventID = time.Now().UnixNano() // Генерация уникального идентификатора события
	return s.storage.SaveEvent(e)
}
