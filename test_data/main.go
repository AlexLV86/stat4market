package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"time"

	_ "github.com/ClickHouse/clickhouse-go"
)

type Event struct {
	EventID   int64     `db:"eventID"`
	EventType string    `db:"eventType"`
	UserID    int64     `db:"userID"`
	EventTime time.Time `db:"eventTime"`
	Payload   string    `db:"payload"`
}

func main() {
	connect, err := sql.Open("clickhouse", "tcp://localhost:9000?debug=true")
	if err != nil {
		log.Fatal(err)
	}
	if err := connect.Ping(); err != nil {
		log.Fatal(err)
	}

	insertDataFor60Days(connect)

	// Запрос события
	eventType := "event1"
	startTime := time.Now().AddDate(0, 0, -60).Format("2006-01-02 15:04:05")
	endTime := time.Now().Format("2006-01-02 15:04:05")
	events, err := selectEvents(connect, eventType, startTime, endTime)
	if err != nil {
		log.Fatal(err)
	}
	for _, e := range events {
		fmt.Printf("EventID: %d, EventType: %s, UserID: %d, EventTime: %s, Payload: %s\n", e.EventID, e.EventType, e.UserID, e.EventTime.Format("2006-01-02 15:04:05"), e.Payload)
	}
}

func insertDataFor60Days(connect *sql.DB) {
	ctx := context.Background()
	tx, err := connect.BeginTx(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	stmt, err := tx.Prepare("INSERT INTO events (eventID, eventType, userID, eventTime, payload) VALUES (?, ?, ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	for day := -60; day <= 0; day++ {
		currentDay := time.Now().AddDate(0, 0, day)
		eventsCount := rand.Intn(9) + 1 // От 1 до 10 событий в день

		for event := 0; event < eventsCount; event++ {
			eventType := fmt.Sprintf("event%d", rand.Intn(10)+1) // Случайный тип события от event1 до event10
			userID := rand.Int63n(100) + 1                       // Случайный userID от 1 до 100
			payload := fmt.Sprintf("{%s_field:%d_value}", eventType, userID)
			eventTime := currentDay
			eventID := time.Now().UnixNano() // Генерация уникального идентификатора события
			if _, err := stmt.Exec(eventID, eventType, userID, eventTime, payload); err != nil {
				log.Fatal(err)
			}
		}
		fmt.Printf("Inserted %d events for %s\n", eventsCount, currentDay.Format("2006-01-02"))
	}

	// Завершаем транзакцию
	if err := tx.Commit(); err != nil {
		log.Fatal(err)
	}
	fmt.Println("Data insertion for 60 days completed.")
}

func selectEvents(connect *sql.DB, eventType string, startTime string, endTime string) ([]Event, error) {
	query := "SELECT eventID, eventType, userID, eventTime, payload FROM events WHERE eventType = ? AND eventTime BETWEEN ? AND ?"
	rows, err := connect.Query(query, eventType, startTime, endTime)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var events []Event
	for rows.Next() {
		var e Event

		if err := rows.Scan(&e.EventID, &e.EventType, &e.UserID, &e.EventTime, &e.Payload); err != nil {
			return nil, err
		}
		events = append(events, e)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return events, nil
}
