module tests/stat4market/test_data

go 1.22.0

require github.com/ClickHouse/clickhouse-go v1.5.4

require github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58 // indirect
